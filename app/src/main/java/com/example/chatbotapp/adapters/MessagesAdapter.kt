package com.example.chatbotapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.chatbotapp.R
import com.example.chatbotapp.models.MessageData
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MessagesAdapter(val data: List<MessageData>) :
    RecyclerView.Adapter<MessagesAdapter.ViewHolder>() {

    private val sdf: SimpleDateFormat = SimpleDateFormat("hh:mm:ss aa, d MMM yyyy", Locale.US)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutId: Int
        if (viewType == 0)
            layoutId = R.layout.adapter_msg_self
        else
            layoutId = R.layout.adapter_msg_other
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                layoutId,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position].isSelf) return 0 else 1
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.tvMsg.text = item.message
        holder.tvDate.text = sdf.format(Date(item.created_at))
        holder.tvName.text = item.senderName
        holder.imgSent?.visibility = if (item.isSelf && item.isSynced) View.VISIBLE else View.GONE
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName: TextView = view.findViewById(R.id.tvName)
        var tvDate: TextView = view.findViewById(R.id.tvDate)
        var tvMsg: TextView = view.findViewById(R.id.tvMsg)
        var imgSent: ImageView? = view.findViewById(R.id.imgSent)
    }

}
