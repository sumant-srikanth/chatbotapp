package com.example.chatbotapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.chatbotapp.R
import com.example.chatbotapp.models.ChatData
import com.example.chatbotapp.models.MessageData
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ChatsAdapter(val data: ArrayList<ChatData>, val onChatClicked: (ChatData) -> Unit) :
    RecyclerView.Adapter<ChatsAdapter.ViewHolder>() {

    private var selectedChatId: Long = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.adapter_chat,
                parent,
                false
            )
        )
    }

    fun updateSelectedChat(chatId: Long) {
        selectedChatId = chatId
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.tvName.text = item.name
        if (selectedChatId == item.id) {
            holder.parent.setBackgroundResource(R.drawable.bg_rounded_rect_green)
        } else {
            holder.parent.setBackgroundResource(0)
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName: TextView = view.findViewById(R.id.tvName)
        var parent: View = view.findViewById(R.id.parent)

        init {
            view.setOnClickListener {
                onChatClicked(data[adapterPosition])
            }
        }
    }

}
