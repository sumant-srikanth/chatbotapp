package com.example.chatbotapp.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class APIClient {

    companion object {

        private const val CONNECT_TIMEOUT_SECONDS: Long = 15
        private const val READ_TIMEOUT_SECONDS: Long = 15
        private const val WRITE_TIMEOUT_SECONDS: Long = 15

        private fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            return interceptor
        }

        private fun provideInterceptor(): Interceptor {
            return Interceptor { chain ->
                val original = chain.request()
                val requestBuilder = original.newBuilder()
                val request = requestBuilder.build()
                chain.proceed(request)
            }
        }

        private fun getHttpClient(
            httpLoggingInterceptor: HttpLoggingInterceptor,
            interceptor: Interceptor
        ): OkHttpClient {
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(httpLoggingInterceptor)
            httpClient.addInterceptor(interceptor)
            httpClient.connectTimeout(CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            httpClient.readTimeout(READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            httpClient.writeTimeout(WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            return httpClient.build()
        }

        fun getRetrofit(baseUrl: String): Retrofit {
            return Retrofit.Builder().baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(
                    getHttpClient(
                        provideHttpLoggingInterceptor(),
                        provideInterceptor()
                    )
                )
                .build()
        }
    }
}