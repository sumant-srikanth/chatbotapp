package com.example.chatbotapp.api

import com.example.chatbotapp.models.MessageResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ChatAPIService {
    @GET("api/chat")
    fun sendMessage(
        @Query("message") message: String,
        @Query("externalID") externalID: String,
        @Query("chatBotID") chatBotID: String,
        @Query("apiKey") apiKey: String
    ): Call<MessageResponse>
}