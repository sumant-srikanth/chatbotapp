package com.example.chatbotapp.models

import java.io.Serializable

class MessageResponse(
    val success: Int,
    val message: MessageResponseBody,
    val errorMessage: String
) : Serializable