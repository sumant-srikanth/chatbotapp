package com.example.chatbotapp.models

import java.io.Serializable

class MessageResponseBody(
    val chatBotName: String,
    val chatBotID: Long,
    val message: String,
    val emotion: String
) : Serializable