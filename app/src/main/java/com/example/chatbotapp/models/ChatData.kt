package com.example.chatbotapp.models

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "chats")
data class ChatData(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val name: String,
    val created_at: Long
) {
}