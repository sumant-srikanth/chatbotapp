package com.example.chatbotapp.models

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "messages",
    foreignKeys = [ForeignKey(
        entity = ChatData::class,
        parentColumns = ["id"],
        childColumns = ["chatId"]
    )]
)
data class MessageData(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val message: String,
    val chatId: Long,
    val senderName: String,
    val isSelf: Boolean,
    val created_at: Long
) {
    var isSynced: Boolean = false
}