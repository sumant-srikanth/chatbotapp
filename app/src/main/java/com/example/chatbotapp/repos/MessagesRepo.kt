package com.example.chatbotapp.repos

import android.os.AsyncTask
import com.example.chatbotapp.Utils
import com.example.chatbotapp.api.APIClient
import com.example.chatbotapp.api.ChatAPIService
import com.example.chatbotapp.db.AppDB
import com.example.chatbotapp.db.ChatsDao
import com.example.chatbotapp.db.MessagesDao
import com.example.chatbotapp.models.ChatData
import com.example.chatbotapp.models.MessageData
import com.example.chatbotapp.models.MessageResponse
import com.example.chatbotapp.workers.MessageSyncWorker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class MessagesRepo {

    companion object {
        const val CHATBOT_ID = "63906"
        const val API_KEY = "6nt5d1nJHkqbkphe"
    }

    private val apiClient = APIClient.getRetrofit(Utils.BASE_URL)
        .create(ChatAPIService::class.java)
    private val db = AppDB.getInstance()

    // ========= FETCH CHATS ==========
    fun fetchChats(onSuccess: (List<ChatData>) -> Unit) {
        FetchChatsTask(db.getChatsDao(), onSuccess).execute()
    }

    class FetchChatsTask(val chatsDao: ChatsDao, val onSuccess: (List<ChatData>) -> Unit) :
        AsyncTask<Void, Void, List<ChatData>>() {

        override fun doInBackground(vararg p0: Void?): List<ChatData> {
            return chatsDao.getChats()
        }

        override fun onPostExecute(result: List<ChatData>) {
            onSuccess(result)
        }
    }

    // ========= INSERT CHAT ==========
    fun addChat(name: String, onSuccess: (ChatData) -> Unit) {
        InsertChatTask(name, db.getChatsDao(), onSuccess).execute()
    }

    class InsertChatTask(
        val name: String,
        val chatsDao: ChatsDao,
        val onSuccess: (ChatData) -> Unit
    ) :
        AsyncTask<Void, Void, ChatData?>() {
        override fun doInBackground(vararg p0: Void?): ChatData? {
            try {
                val chat = ChatData(0, name, Calendar.getInstance().time.time)
                val result = chatsDao.addChat(chat)
                if (result > 0) {
                    return chat.copy(id = result)
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return null;
        }

        override fun onPostExecute(result: ChatData?) {
            println(">>>>>> ADD CHAT STATUS == ${result}")
            result?.let {
                onSuccess(it)
            }
        }
    }

    // ========= INSERT MESSAGE ==========
    fun addMessage(
        message: MessageData,
        onSuccess: (MessageData) -> Unit
    ) {
        InsertMessageTask(message, db.getMessagesDao(), onSuccess).execute()
    }

    class InsertMessageTask(
        val msg: MessageData,
        val messagesDao: MessagesDao,
        val onSuccess: (MessageData) -> Unit
    ) :
        AsyncTask<Void, Void, MessageData?>() {
        override fun doInBackground(vararg p0: Void?): MessageData? {
            try {
                val msg = MessageData(
                    0,
                    msg.message,
                    msg.chatId,
                    msg.senderName,
                    msg.isSelf,
                    Calendar.getInstance().time.time
                )
                val result = messagesDao.addMessage(msg)
                if (result > 0) {
                    return msg.copy(id = result)
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return null;
        }

        override fun onPostExecute(result: MessageData?) {
            result?.let {
                onSuccess(it)
            }
        }
    }

    // ====== FETCH MESSAGES ====
    fun fetchMessages(chatId: Long, onSuccess: (List<MessageData>?) -> Unit) {
        FetchMessagesTask(chatId, db.getMessagesDao(), onSuccess).execute()
    }

    class FetchMessagesTask(
        val chatId: Long,
        val messagesDao: MessagesDao,
        val onSuccess: (List<MessageData>?) -> Unit
    ) :
        AsyncTask<Void, Void, List<MessageData>?>() {

        override fun doInBackground(vararg p0: Void?): List<MessageData>? {
            try {
                return messagesDao.getMessages(chatId)
            } catch (ex: Exception) {
                ex.printStackTrace()
                return null
            }
        }

        override fun onPostExecute(result: List<MessageData>?) {
            result?.let {
                onSuccess(it)
            }
        }
    }

    // =========== UPDATE MESSAGE STATUS ============
    fun updateMessage(
        message: MessageData,
        onComplete: (Boolean) -> Unit
    ) {
        UpdateMessageTask(message, db.getMessagesDao(), onComplete).execute()
    }

    class UpdateMessageTask(
        val msg: MessageData,
        val messagesDao: MessagesDao,
        val onComplete: (Boolean) -> Unit
    ) : AsyncTask<Void, Void, Boolean>() {
        override fun doInBackground(vararg p0: Void?): Boolean {
            try {
                messagesDao.updateMessage(msg)
                return true
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return false
        }

        override fun onPostExecute(result: Boolean) {
            onComplete(result)
        }
    }

    // ========= SYNCHRONOUS METHODS FOR WORKER ==============
    fun getUnsyncedMessages(): List<MessageData> {
        return db.getMessagesDao().getUnSyncedMessages()
    }

    fun syncMessage(
        myMessage: MessageData,
        onBotReplySuccess: (MessageData) -> Unit,
        onBotReplyFailure: (Throwable, Boolean) -> Unit
    ) {
        val call = apiClient.sendMessage(
            myMessage.message,
            myMessage.senderName,
            CHATBOT_ID,
            API_KEY
        )

        try {
            val response = call.execute()
            response.body().let {
                response.body().let { responseBody ->
                    val replyMessage = responseBody?.message
                    if (replyMessage != null) {
                        val messagesDao = db.getMessagesDao()

                        // Update sync flag in DB
                        myMessage.isSynced = true
                        messagesDao.updateMessage(myMessage)

                        val botMsg = MessageData(
                            0,
                            replyMessage.message,
                            myMessage.chatId,
                            replyMessage.chatBotName,
                            false,
                            Calendar.getInstance().time.time
                        ).apply {
                            isSynced = true // Replies are always considered synced
                        }

                        messagesDao.addMessage(botMsg)

                        onBotReplySuccess(botMsg)
                    } else {
                        onBotReplyFailure(Exception(responseBody!!.errorMessage), false)
                    }
                }
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
            onBotReplyFailure(ex, false)
        }
    }
    // ================================================

    fun sendMessage(
        msg: MessageData,
        onLocalMessageAddSuccess: (MessageData) -> Unit,
        onLocalMessageSynced: (MessageData) -> Unit,
        onBotReplySuccess: (MessageData) -> Unit,
        onBotReplyFailure: (Throwable, Boolean) -> Unit
    ) {
        addMessage(msg) { myMessage ->
            // Local message has been stored
            onLocalMessageAddSuccess(myMessage)

            // Send message to API and await bot reply - on success, update sync flag for our local message AND insert bot's message
            val call = apiClient.sendMessage(
                myMessage.message,
                myMessage.senderName,
                CHATBOT_ID,
                API_KEY
            )
            call.enqueue(object :
                Callback<MessageResponse> {
                override fun onResponse(
                    call: Call<MessageResponse>,
                    response: Response<MessageResponse>
                ) {
                    try {
                        response.body().let { responseBody ->
                            val replyMessage = responseBody?.message
                            if (replyMessage != null) {
                                // Update sync flag in DB
                                myMessage.isSynced = true
                                updateMessage(myMessage) { isUpdated ->
                                    onLocalMessageSynced(myMessage)
                                }

                                val botMsg = MessageData(
                                    0,
                                    replyMessage.message,
                                    myMessage.chatId,
                                    replyMessage.chatBotName,
                                    false,
                                    Calendar.getInstance().time.time
                                ).apply {
                                    isSynced = true // Replies are always considered synced
                                }

                                addMessage(botMsg) {
                                    onBotReplySuccess(it)
                                }
                            } else {
                                onBotReplyFailure(Exception(responseBody!!.errorMessage), false)
                                MessageSyncWorker.enqueue()
                            }
                        }
                    } catch (ex: Exception) {
                        onBotReplyFailure(ex, false)
                        MessageSyncWorker.enqueue()
                    }
                }

                override fun onFailure(call: Call<MessageResponse>, t: Throwable) {
                    onBotReplyFailure(t, call.isCanceled)
                    MessageSyncWorker.enqueue()
                }
            })
        }
    }
}