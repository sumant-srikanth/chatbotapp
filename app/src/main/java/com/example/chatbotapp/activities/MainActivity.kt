package com.example.chatbotapp.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chatbotapp.R
import com.example.chatbotapp.Utils
import com.example.chatbotapp.adapters.ChatsAdapter
import com.example.chatbotapp.adapters.MessagesAdapter
import com.example.chatbotapp.models.ChatData
import com.example.chatbotapp.models.MessageData
import com.example.chatbotapp.repos.MessagesRepo
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_nav_menu.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    private var curName = "Me"
    private var curChatId: Long = 0

    private val repo = MessagesRepo()
    private lateinit var messagesAdapter: MessagesAdapter
    private lateinit var chatsAdapter: ChatsAdapter

    private val chatList = ArrayList<ChatData>()
    private val messageList = ArrayList<MessageData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setup UI
        img_nav_menu.setOnClickListener(View.OnClickListener {
            drawer_layout.openDrawer(nav_menu)
        })

        btn_send.setOnClickListener {
            sendMsg(et_input.text.toString())
        }

        btn_add_chat.setOnClickListener {
            addNewChat()
        }

        chatsAdapter = ChatsAdapter(chatList) {
            if (onChatSelected(it)) {
                Utils.showSnackbar(this@MainActivity, "${it.name} chat selected!")
            }
        }
        rv_chats.layoutManager = LinearLayoutManager(this)
        rv_chats.adapter = chatsAdapter

        messagesAdapter = MessagesAdapter(messageList)
        rv_messages.layoutManager = LinearLayoutManager(this)
        rv_messages.itemAnimator = DefaultItemAnimator()
        rv_messages.adapter = messagesAdapter

        // Fetch Data
        repo.fetchChats() {
            chatList.clear()
            chatList.addAll(it)
            chatsAdapter.notifyDataSetChanged()

            if (chatList.size > 0) {
                onChatSelected(chatList[0])
            } else {
                addNewChat()
            }
        }

        // Sync receiver
        try {
            val intentFilter = IntentFilter().apply { addAction(Utils.ACTION_SYNC_MESSAGE_UPDATE) }
            registerReceiver(
                syncedMessageReceiver,
                intentFilter
            )
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun onChatSelected(chat: ChatData): Boolean {
        if (chat.id != curChatId) {
            curChatId = chat.id
            chatsAdapter.updateSelectedChat(chat.id)
            fetchMessages(curChatId)
            return true
        }
        return false
    }

    fun fetchMessages(chatId: Long) {
        repo.fetchMessages(chatId) {
            messageList.clear()
            it?.let {
                messageList.addAll(it)
            }
            messagesAdapter.notifyDataSetChanged()

            scrollToLast()
        }
    }

    fun addNewChat() {
        repo.addChat("Chat ${chatList.size + 1}") {
            chatList.add(it)
            chatsAdapter.notifyDataSetChanged()

            onChatSelected(it)
        }
    }

    fun sendMsg(text: String) {
        if (text.isEmpty()) {
            Utils.showSnackbar(this, "Please enter message!")
            return
        }

        toggleInput(false)

        val msg = MessageData(0, text, curChatId, curName, true, Calendar.getInstance().time.time)
        repo.sendMessage(msg,
            {
                toggleInput(true, true)
                addMessageToList(it)
            },
            {
                updateMessage(it)
            },
            {
                addMessageToList(it)
            }, { t, isCancelled ->
                if (!isCancelled) {
                    //Utils.showSnackbar(this@MainActivity, t.toString())
                }
            })
    }

    fun addMessageToList(msg: MessageData) {
        messageList.add(msg)
        messagesAdapter.notifyItemInserted(messageList.size)

        scrollToLast()
    }

    fun scrollToLast() {
        if (messageList.size > 0)
            rv_messages.smoothScrollToPosition(messagesAdapter.itemCount - 1)
    }

    fun updateMessage(updateMessage: MessageData) {
        if (updateMessage.chatId == curChatId) {
            messageList.forEachIndexed { pos, obj ->
                if (obj.id == updateMessage.id) {
                    messageList[pos] = updateMessage
                    messagesAdapter.notifyItemChanged(pos)
                    return
                }
            }
        }
    }

    fun toggleInput(isEnabled: Boolean, clearInput: Boolean = false) {
        et_input.isEnabled = isEnabled
        btn_send.isEnabled = isEnabled

        if (clearInput) {
            et_input.text.clear()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            unregisterReceiver(syncedMessageReceiver)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private val syncedMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (context != null && intent != null) {
                val chatIds: ArrayList<Long> =
                    intent.getSerializableExtra(Utils.EXTRA_SYNCED_CHAT_IDS) as ArrayList<Long>
                if (chatIds.contains(curChatId)) {
                    fetchMessages(curChatId)
                }
            }
        }
    }
}
