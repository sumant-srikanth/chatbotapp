package com.example.chatbotapp

import android.app.Activity
import com.google.android.material.snackbar.Snackbar

class Utils {
    companion object {
        const val BASE_URL = "http://www.personalityforge.com/"

        const val ACTION_SYNC_MESSAGE_UPDATE = "com.example.chatbotapp.ACTION_SYNC_MESSAGE_UPDATE"
        const val EXTRA_SYNCED_CHAT_IDS = "EXTRA_SYNCED_CHAT_IDS"

        fun showSnackbar(activity: Activity, text: String) {
            Snackbar.make(activity.findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG)
                .show()
        }

    }
}