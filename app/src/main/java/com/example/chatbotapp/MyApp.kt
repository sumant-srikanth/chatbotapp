package com.example.chatbotapp

import android.app.Application
import android.content.Context
import com.example.chatbotapp.db.AppDB

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        INSTANCE = this

        AppDB.initialize(this)
    }

    companion object {
        private lateinit var INSTANCE: MyApp

        fun getAppContext(): Context {
            return INSTANCE.applicationContext
        }
    }
}