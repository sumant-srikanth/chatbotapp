package com.example.chatbotapp.workers

import android.content.Context
import android.content.Intent
import androidx.work.*
import com.example.chatbotapp.Utils
import com.example.chatbotapp.repos.MessagesRepo
import java.util.concurrent.TimeUnit

class MessageSyncWorker(val context: Context, workerParameters: WorkerParameters) :
    Worker(context, workerParameters) {

    companion object {
        val NAME = "MESSAGE_SYNC_WORKER"

        @JvmStatic
        fun enqueue() {
            val constraints =
                Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
            val workRequest = OneTimeWorkRequest.Builder(MessageSyncWorker::class.java)
                .setInitialDelay(5000, TimeUnit.MILLISECONDS)
                .setBackoffCriteria(BackoffPolicy.EXPONENTIAL, 10000, TimeUnit.MILLISECONDS)
                .setConstraints(constraints)
                .build()

            val workManager = WorkManager.getInstance()
            workManager.enqueueUniqueWork(NAME, ExistingWorkPolicy.REPLACE, workRequest)
        }

    }

    override fun doWork(): Result {
        val repo = MessagesRepo()
        val unsyncedMessages = repo.getUnsyncedMessages()

        var shouldRetry = false
        val syncedChatIds = ArrayList<Long>()

        unsyncedMessages.forEach {
            repo.syncMessage(it, {
                syncedChatIds.add(it.chatId)
            }, { t, isCancelled ->
                t.printStackTrace()
                shouldRetry = true
            })

            try {
                Thread.sleep(1000)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        if (syncedChatIds.size > 0) {
            val intent = Intent(Utils.ACTION_SYNC_MESSAGE_UPDATE)
            intent.putExtra(Utils.EXTRA_SYNCED_CHAT_IDS, syncedChatIds)
            context.sendBroadcast(intent)
        }

        if (shouldRetry)
            return Result.retry()
        return Result.success()
    }

}