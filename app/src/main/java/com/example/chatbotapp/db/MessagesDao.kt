package com.example.chatbotapp.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.chatbotapp.models.MessageData

@Dao
interface MessagesDao {

    @Insert
    fun addMessage(data: MessageData): Long

    @Update
    fun updateMessage(data: MessageData)

    @Query("SELECT * FROM messages WHERE chatId=:chatId ORDER BY id ASC")
    fun getMessages(chatId: Long): List<MessageData>

    @Query("SELECT * FROM messages WHERE isSynced=0 AND isSelf=1")
    fun getUnSyncedMessages(): List<MessageData>
}