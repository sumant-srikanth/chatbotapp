package com.example.chatbotapp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.chatbotapp.models.ChatData
import com.example.chatbotapp.models.MessageData

@Database(
    entities = arrayOf(MessageData::class, ChatData::class),
    version = 1,
    exportSchema = false
)
abstract class AppDB : RoomDatabase() {

    abstract fun getMessagesDao(): MessagesDao
    abstract fun getChatsDao(): ChatsDao

    companion object {
        val DB_NAME = "db_chatbot"
        private var appDb: AppDB? = null

        @JvmStatic
        fun initialize(context: Context) {
            appDb = Room.databaseBuilder(
                context, AppDB::class.java,
                DB_NAME
            )
                .addMigrations()
                .fallbackToDestructiveMigration()
                .build()
        }

        @JvmStatic
        @Throws(IllegalStateException::class)
        fun getInstance(): AppDB {
            if (appDb == null) {
                throw IllegalStateException("Database must be initialized!")
            }
            return appDb!!
        }
    }
}