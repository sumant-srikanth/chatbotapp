package com.example.chatbotapp.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.chatbotapp.models.ChatData

@Dao
interface ChatsDao {

    @Insert
    fun addChat(data: ChatData): Long

    @Query("SELECT * FROM chats ORDER BY id ASC")
    fun getChats(): List<ChatData>
}